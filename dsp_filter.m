function dsp_filter(filterobj, infile, outfile)


order = filtord(filterobj);
padperside = 5*order;

hdr = rdxwavhd(infile);
[dSubchunkSizePosn, dataPosn] = copy_xwav_hdr(outfile, hdr);

% Assume that we are dealing with xwavs for now
ftype = 2;  %
if ftype == 2
    
    % We should be able to open the files for reading and writing as
    % we already copied the headers, but check just in case...
    
    in_h = fopen(infile, 'r');
    if in_h == -1
        error('Unable to open %s for reading', infile);
    end
    
    out_h = fopen(outfile, 'r+');
    if out_h == -1
        error('Unable to open %s for reading/writing', outfile);
    end
    
    
    % Determine sample size
    bytes = hdr.nBits / 8;
    switch hdr.nBits
        case 16
            dtype = 'int16';
        case 24
            dtype = 'int24';
        case 32
            dtype = 'int32';
        otherwise
            error('Unsupported sample bit depth:  %d', hdr.nbits)
    end
    
    % interval between end of previous raw file and start of new
    % Used to determine if we can use the previous filter state.
    raw_deltas = hdr.raw.dnumStart(2:end) - hdr.raw.dnumEnd(1:end-1);
    raw_deltas_s = seconds(raw_deltas);
    % We consider the data to be contiguous if the difference between
    % the end time of the previous and start of the next is no more
    % than the time of one sample with a pad for rounding error.
    tolerance_s = 1.5 * 1/hdr.fs;  
    
    % Process each raw file
    next = [];
    current = [];
    padded = [];  % data block
    count = 0;
    debug = true;
    if debug
        % Rotating history of raw, padded, and fitlered data
        histN = 3;
        rawhist = cell(histN, 1);
        padhist = cell(histN, 1);
        filthist = cell(histN, 1);
        outhist = cell(histN, 1);
    end
    for ridx = 1:hdr.xhd.NumOfRawFiles
        % store previous (may be empty) and read raw file(ridx)
        prior = current;
        if debug
            histidx = rem(ridx-1, histN)+1;
            rawhist{histidx} = current;
        end
        if isempty(next)
            % first one or first after a gap
            current = read_rawfile(in_h, hdr, bytes, dtype, ridx)';
        else
            current = next;  % already have it
        end
        % Get the subsequent raw file (ridx+1) if available
        if ridx < hdr.xhd.NumOfRawFiles && raw_deltas_s(ridx) < tolerance_s
            next = read_rawfile(in_h, hdr, bytes, dtype, ridx+1)';
            if debug
                rawhist{rem(histidx+1, histN)+1} = next;
            end
        else
            next = [];  % no more data or there is a gap
        end
        
        % Add beginning/ending transients
        [samples, ch] = size(current);
        if ch > 1
            warning([ 'Code was never tested with multichannel.  ',...
                'Verify output, we may need transposes']);
        end
        
        % Ensure pad matrix appropriate size
        expected_pad = [2*padperside + samples, ch];
        if ~ all(size(padded) == expected_pad)
            padded = zeros(expected_pad);  
        end
        
        % prior raw file pad
        if ~ isempty(prior)
            padded(1:padperside, :) = prior(end-padperside+1:end, :);
        else
            % no prior raw file, reflect
            padded(1:padperside, :) = current(padperside:-1:1, :);
        end
        % raw file
        padded(padperside+1:end-padperside,:) = current;
        % subsequent raw file
        if ~ isempty(next)
            padded(end-padperside+1:end,:) = next(1:padperside, :);
        else
            padded(end-padperside+1:end,:) = current(end:-1:end-padperside+1,:);
        end
        
        filtered = filtfilt(filterobj, padded);
        retain = filtered(padperside+1:end-padperside,:)';

        if debug
            padhist{histidx} = padded;
            filthist{histidx} = filtered;
            outhist{histidx} = retain;
        end
        
        ridx_count = write_rawfile(out_h, hdr, dtype, ridx, retain);
        count = count + ridx_count;
    end % end raw file loop
    
    % clean up our toys...
    if fseek(out_h, dSubchunkSizePosn, -1)
        error('Unable to seek to data chunk size location %s', ...
            dSubchunkSizePosn);
    else
        % Write count samples in bytes
        sample_bytes = count * bytes; 
        % This should be true unless we change sample rate
        assert(hdr.xhd.dSubchunkSize == sample_bytes, ...
            'Bytes written %d ~= bytes from source %d: %s', ...
            sample_bytes, hdr.xhd.dSubchunkSize, infile);
        fwrite(out_h, sample_bytes, 'uint32');
    end
    fclose(out_h);
    fclose(in_h);
end % end if ftype


function data = read_rawfile(in_h, hdr, bytes, dtype, i)
% data = read_rawfile(in_h, hdr, bytes, i)
% Given an open file descriptor to an xwav file (in_h), its header (hdr),
% the number of bytes per sample (bytes), and the data data type (dtype),
% read the ith raw file and return its data.

in_start = hdr.xhd.byte_loc(i);  % starting byte of data
status = fseek(in_h, in_start, -1);  % position relative to start of file
if status ~= 0
    error('Unable to seek read file to position %d', in_start);
end
% determine how many samples and read the data
samples = hdr.xhd.byte_length(i) / (bytes * hdr.nch);
data = fread(in_h, [hdr.nch, samples], dtype);


function count = write_rawfile(out_h, hdr, dtype, i, data)

out_start = hdr.xhd.byte_loc(i);  % starting byte of output data
status = fseek(out_h, out_start, -1); % position relative to start of file
if status ~= 0
    error('Unable to seek write file to position %d', out_start);
end
count = fwrite(out_h, data, dtype);




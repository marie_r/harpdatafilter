%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% If the remora has been added through the Triton interface, This function 
% is called at the start of every triton session. This file populates 
% toolbars with remora specific options and callbacks.
%
% A best practice for remoras that have multiple m-files containing callbacks
% would be initializing new control windows and buttons here rather than in 
% child m-files.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This should be in ever initialize function. HANDLES is the global
% varible that holds all the graphical buttons and windows of triton
global HANDLES REMORA PARAMS

%Callback expect three arguments.
%Matlab UI apps expect none.  Wrap in a dummy function.
fn = @(x,y,z) harp_data_filter;

% our "Hello World" button is added to the tool menu
REMORA.HarpDataFilter = uimenu(HANDLES.remmenu,'Label','Filter HARP File', ...
                       'Callback', fn);

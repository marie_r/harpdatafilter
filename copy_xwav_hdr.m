function [dSubchunkSizePosn, dataPosn] = copy_xwav_hdr(fname, hdr)
% 2/6/2019
% wrxwavhd(fname, hdr)
% Write an xwav header (hdr) to disk
% The start of the DATA chunk is written, but data themselves are not
%
% dSubchunkSizePosn indicates the seek position where the data count
% has been written based on the existing hdr.  Depending upon what data
% are being written, this may not be correct once the data are written.
%
% dataPosn indicates the seek position where data should start to be
% written.
%
% If this is used for rewriting a file where the number of data
% change, the HARPdir subchunks 



fid =  fopen(fname,'w');
if fid == -1
    error('Unabel to open %s for writing', fname);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% RIFF chunk
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fwrite(fid, hdr.xhd.ChunkID, 'uchar');             % "RIFF"
fwrite(fid, hdr.xhd.ChunkSize,'uint32');           % File size - 8 bytes
fwrite(fid,hdr.xhd.Format,'uchar');                % "WAVE"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Format Subchunk
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fwrite(fid,hdr.xhd.fSubchunkID,'uchar');    % "fmt "
fwrite(fid,hdr.xhd.fSubchunkSize,'uint32');        % (Size of Subchunk - 8) = 16 bytes (PCM)
fwrite(fid,hdr.xhd.AudioFormat,'uint16');         % Compression code (PCM = 1)
fwrite(fid,hdr.xhd.NumChannels,'uint16');         % Number of Channels
fwrite(fid,hdr.xhd.SampleRate,'uint32');          % Sampling Rate (samples/second)
fwrite(fid,hdr.xhd.ByteRate,'uint32');            % Byte Rate = SampleRate * NumChannels * BitsPerSample / 8
fwrite(fid,hdr.xhd.BlockAlign,'uint16');          % # of Bytes per Sample Slice = NumChannels * BitsPerSample / 8
fwrite(fid,hdr.xhd.BitsPerSample,'uint16');       % # of Bits per Sample : 8bit = 8, 16bit = 16, etc


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% HARP Subchunk
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~strcmp(hdr.xhd.hSubchunkID,'harp')
  error('Bad header, harp subchunk is not correct');
end

fwrite(fid,hdr.xhd.hSubchunkID,'uchar');          % "harp"
fwrite(fid,hdr.xhd.hSubchunkSize,'uint32');       % (Size of Subchunk - 8) includes write subchunk
fwrite(fid,hdr.xhd.WavVersionNumber,'uchar');     % Version number of the "harp" header (0-255)
fwrite(fid,hdr.xhd.FirmwareVersionNumber,'uchar');   % HARP Firmware Vesion
fwrite(fid,hdr.xhd.InstrumentID,'uchar');            % Instrument ID Number (0-255)
fwrite(fid,hdr.xhd.SiteName,'uchar');             % Site Name, 4 alpha-numeric characters
fwrite(fid,hdr.xhd.ExperimentName,'uchar');       % Experiment Name
fwrite(fid,hdr.xhd.DiskSequenceNumber,'uchar');   % Disk Sequence Number (1-16)
fwrite(fid,hdr.xhd.DiskSerialNumber,'uchar');     % Disk Serial Number
fwrite(fid,hdr.xhd.NumOfRawFiles,'uint16');       % Number of RawFiles in XWAV file
fwrite(fid,hdr.xhd.Longitude,'int32');            % Longitude (+/- 180 degrees) * 100,000
fwrite(fid,hdr.xhd.Latitude,'int32');             % Latitude (+/- 90 degrees) * 100,000
fwrite(fid,hdr.xhd.Depth,'int16');                % Depth, positive == down
if hdr.xhd.WavVersionNumber == 2
    fwrite(fid,hdr.xhd.drate,'single');
end
fwrite(fid,hdr.xhd.Reserved,'uchar');            % Padding to extend subchunk to 64 bytes

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% write sub-sub chunk
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i = 1:hdr.xhd.NumOfRawFiles
                                                        % Start of Raw file :
    fwrite(fid,hdr.xhd.year(i),'uchar');          % Year
    fwrite(fid,hdr.xhd.month(i),'uchar');         % Month
    fwrite(fid,hdr.xhd.day(i),'uchar');           % Day
    fwrite(fid,hdr.xhd.hour(i),'uchar');          % Hour
    fwrite(fid,hdr.xhd.minute(i),'uchar');        % Minute
    fwrite(fid,hdr.xhd.secs(i),'uchar');          % Seconds
    fwrite(fid,hdr.xhd.ticks(i),'uint16');        % Milliseconds
    fwrite(fid,hdr.xhd.byte_loc(i),'uint32');     % Byte location in xwav file of RawFile start
    fwrite(fid,hdr.xhd.byte_length(i),'uint32');  % Byte length of RawFile in xwav file
    fwrite(fid,hdr.xhd.write_length(i),'uint32'); % # of blocks in RawFile length (default = 60000)
    fwrite(fid,hdr.xhd.sample_rate(i),'uint32');  % sample rate of this RawFile
    fwrite(fid,hdr.xhd.gain(i),'uint8');          % gain (1 = no change)
    fwrite(fid,hdr.xhd.padding,'uchar');    % Padding to make it 32 bytes...misc info can be added here
    if hdr.xhd.WavVersionNumber == 2
        fwrite(fid,hdr.xhd.dt(i),'single');     % time diff to channel 1
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DATA Subchunk
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Write out the start of the DATA subchunk
fwrite(fid,hdr.xhd.dSubchunkID,'uchar');    % "data"
% Note the byte location of the unsigned 32 bit sample count
dSubchunkSizePosn = ftell(fid);
hdr.xhd.dSubchunkSize = fwrite(fid,1,'uint32');        % (Size of Subchunk - 8) includes write subchunk
% Note the byte location where data will be written
dataPosn = ftell(fid);
fclose(fid);

